package com.fraction;

public class Fraction {

    private int num, den;

    public int getNum () {
        return this.num;
    }

    public int getDen () {
        return this.den;
    }

    public void printFraction () {
        if (this.den == 1 || this.num == 0)
            System.out.println(this.getNum());
        else
            System.out.println(this.getNum() + "/" + this.getDen());
    }

    public Fraction add (Fraction fracEx) {
         Fraction fracRes = new Fraction();
         fracRes.num = this.num*fracEx.den + this.den*fracEx.num;
         fracRes.den = den*fracEx.den;
         fracRes.cancel();
         return fracRes;
    }

    public Fraction sub (Fraction fracEx) {
         Fraction fracRes = new Fraction();
         fracRes.num = this.num*fracEx.den - this.den*fracEx.num;
         fracRes.den = this.den*fracEx.den;
         fracRes.cancel();
         return fracRes;
    }

    public Fraction mul (Fraction fracEx) {
         Fraction fracRes = new Fraction();
         fracRes.num = this.num*fracEx.num;
         fracRes.den = this.den*fracEx.den;
         fracRes.cancel();
         return fracRes;
    }

    public Fraction div (Fraction fracEx) {
        Fraction fracRes = new Fraction();
        fracRes.num = this.num*fracEx.den;
        fracRes.den = this.den*fracEx.num;
        fracRes.cancel();
        return fracRes;
    }

    public void cancel () {
        int a = this.num, b = this.den, gcd;


        while (a!=0 && b!=0) {

            if (a > b)
                a = a % b;
            else
                b = b % a;
        }
        gcd = a + b;

        this.num /= gcd;
        this.den /= gcd;
    }

    public Fraction() {

    }

    public Fraction(int numerator, int denominator) {
        this.num = numerator;
        this.den = denominator;

        int help = this.num / this.den;
    }

}
