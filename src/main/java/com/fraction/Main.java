package com.fraction;

public class Main {
    public static void main(String[] args) {
        try {
            Fraction first = new Fraction(17,3);
            Fraction second = new Fraction(9,0);
            Fraction third = new Fraction();
            third = first.mul(second);
            third.printFraction();
        } catch (ArithmeticException ex) {
            System.out.println("В знаменателе дроби обнаружен ноль! " +
                    "Измените входные данные!");
        }
    }
}
