package com.fractiontest;

import com.fraction.Fraction;
import org.junit.Assert;
import org.junit.Test;

public class FractionTest {
    @Test
    public void testGetFraction () {
        Fraction fracOne = new Fraction(2, 3);

        Assert.assertEquals(2, fracOne.getNum());
        Assert.assertEquals(3, fracOne.getDen());
    }

    @Test(expected = ArithmeticException.class)
    public void testInputData () {
        Fraction fracOne = new Fraction(2, 0);
    }

    @Test
    public void testAddFraction () {
        Fraction fracOne = new Fraction(2, 3);
        Fraction fracTwo = new Fraction(4, 3);

        Fraction fracAddResult = fracOne.add(fracTwo);

        Assert.assertEquals(2, fracAddResult.getNum());
        Assert.assertEquals(1, fracAddResult.getDen());
    }

    @Test
    public void testSubFraction () {
        Fraction fracOne = new Fraction(4, 5);
        Fraction fracTwo = new Fraction(2, 7);

        Fraction fracSubResult = fracOne.sub(fracTwo);

        Assert.assertEquals(18, fracSubResult.getNum());
        Assert.assertEquals(35, fracSubResult.getDen());
    }

    @Test
    public void testMulFraction () {
        Fraction fracOne = new Fraction(3, 4);
        Fraction fracTwo = new Fraction(4, 9);

        Fraction fracMulResult = fracOne.mul(fracTwo);

        Assert.assertEquals(1, fracMulResult.getNum());
        Assert.assertEquals(3, fracMulResult.getDen());
    }

    @Test
    public void testDivFraction () {
        Fraction fracOne = new Fraction(4, 9);
        Fraction fracTwo = new Fraction(4, 3);

        Fraction fracDivResult = fracOne.div(fracTwo);

        Assert.assertEquals(1, fracDivResult.getNum());
        Assert.assertEquals(3, fracDivResult.getDen());
    }
}
